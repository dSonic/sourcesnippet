from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'sourcesnippet.views.snippet_world', name='snippet_world'),
    url(r'^signin$', 'sourcesnippet.views.signin', name='signin'),
    url(r'^signout$', 'sourcesnippet.views.signout', name='signout'),
    url(r'^snippet_creator$', 'sourcesnippet.views.snippet_creator', name='snippet_creator'),
    url(r'^my_snippets$', 'sourcesnippet.views.my_snippets', name='my_snippets'),
    # url(r'^blog/', include('blog.urls')), 
    url(r'^admin/', include(admin.site.urls)),
)
