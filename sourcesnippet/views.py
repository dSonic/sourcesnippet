###############################
# Main Views Controllers
###############################

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.utils import simplejson, timezone
from django import forms
from django.forms import ModelForm
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers

from sourcesnippet.models import Snippet, Project, Language

##########################################
# Model form classes for form validation
##########################################
class SigninForm(ModelForm):
	class Meta:
		model = User
		fields = ['password', 'email']

class SnippetCreatorForm(ModelForm):
	class Meta:
		model = Snippet
		fields = ['name', 'description', 'snippet']

##########################################
# Page Views controller starts here
##########################################
@login_required
def snippet_world(request):
	context = {
		'page': 'snippet_world'
	}

	return render(request, 'snippet_world.html', context)

# sign in page
def signin(request):
	context = {}
	# signin user
	if request.method == 'POST':
		form = SigninForm(request.POST) # binding form to user model
		if form.is_valid():
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']

			# verfiy user info
			user = authenticate(username=email, password=password)
			if user is not None:
				if user.is_active:
					# login user and redirect to homepage
					login(request, user)
					return HttpResponseRedirect('/')
				else:
					context['loginError'] = 2
			else:
				context['loginError'] = 1
	else:
		form = SigninForm()

	contextData = {
		'page': 'signin',
		'form': form
	}
	context.update(contextData)

	return render(request, 'signin.html', context)

# logout user
def signout(request):
	logout(request);
	return HttpResponseRedirect('/signin')

# snippet creator page
@login_required
def snippet_creator(request):
	context = {}
	# get languages
	# langs = Language.objects.all()
	langs = Language.objects.values('id', 'name')
	# get projects
	projects = Project.objects.values('id', 'name')

	if request.method == 'POST':
		form = SnippetCreatorForm(request.POST)
		if form.is_valid():
			name = form.cleaned_data['name']
			languageId = request.POST['language']
			projectId = request.POST['project']
			description = form.cleaned_data['description']
			snippet = form.cleaned_data['snippet']

			try:
				isPublic = request.POST['public']
				public = True
			except:
				public = False

			try:
				isImprovement = request.POST['improvement']
				improvement = True
			except:
				improvement = False

			user = request.user
			project = Project.objects.get(pk=projectId)
			language = Language.objects.get(pk=languageId)

			snippetObject = Snippet(name=name, description=description, snippet=snippet, public=public, improvement=improvement, create_date=timezone.now(), update_date=timezone.now(), delete_date=None, liked=0)
			snippetObject.user = user
			snippetObject.project = project
			snippetObject.language = language

			snippetObject.save()

			return HttpResponseRedirect(reverse('snippet_world'))
	else:
		form = SnippetCreatorForm()

	contextData = {
		'page': 'snippet_creator',
		'langs': langs,
		'projects': projects,
		'form': form
	}
	context.update(contextData)

	return render(request, 'snippet_creator.html', context)

# My Snippets View page
@login_required
def my_snippets(request):
	context = {}

	# get snippets
	snippets = Snippet.objects.all()

	context.update({
		'page': 'my_snippets',
		'snippets': snippets,
	})

	return render(request, 'my_snippets.html', context)