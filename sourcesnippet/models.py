##########################
# sourceSnippet model
##########################

from django.db import models
from django.contrib.auth.models import User
from django.db.models import Q

class SnippetManager(models.Manager):
	name = 'snippet manager'

	def __str__(self):
		return self.name

class CommonFields(models.Model):
	name = models.CharField(max_length=25)
	create_date = models.DateTimeField()
	delete_date = models.DateTimeField(null=True, blank=True)

	class Meta:
		abstract = True

class Language(CommonFields):
	language = models.CharField(max_length=25, unique=True)
	user = models.ForeignKey(User)

	def __str__(self):
		return self.name

class Project(CommonFields):
	user = models.ForeignKey(User)

	def __str__(self):
		return self.name

class Snippet(CommonFields):
	description = models.TextField(max_length=500)
	snippet = models.TextField()
	user = models.ForeignKey(User)
	project = models.ForeignKey(Project)
	language = models.ForeignKey(Language)
	public = models.BooleanField()
	improvement = models.BooleanField()
	update_date = models.DateTimeField()
	liked = models.PositiveIntegerField()
	objects = SnippetManager()

	def __str__(self):
		return self.name